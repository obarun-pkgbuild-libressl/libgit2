# Maintainer  : Jean-Michel T.Dydak <jean-michel@obarun.org> <jean-michel@syntazia.org>
#--------------
## PkgSource  : https://www.archlinux.org/packages/extra/x86_64/libgit2/
## Maintainer : Lukas Fleischer <lfleischer@archlinux.org>
## Contributor: Hilton Medeiros <medeiros.hilton@gmail.com>
## Contributor: Dave Reisner <d@falconindy.com>
#--------------------------------------------------------------------------------------

pkgname=libgit2
pkgver=0.28.1
pkgrel=2
epoch=1
arch=('x86_64')
license=('GPL2')
_website="https://libgit2.org"
pkgdesc="A linkable library for Git"
url="https://github.com/libgit2/libgit2"
source=("$pkgname-$pkgver.tar.gz::$url/archive/v$pkgver.tar.gz")

depends=(
    'zlib'
    'libressl'
    'libssh2'
    'curl')
makedepends=(
    'cmake'
    'python')

#--------------------------------------------------------------------------------------
build() {
    cd "$pkgname-$pkgver"

    export LANG=en_US.UTF-8

    cmake \
        -DCMAKE_BUILD_TYPE=Release  \
        -DCMAKE_INSTALL_PREFIX=/usr \
        -DTHREADSAFE=ON
    make
}

check() {
    cd "$pkgname-$pkgver"

    make test
}

package() {
    cd "$pkgname-$pkgver"

    make DESTDIR="$pkgdir" install
}

#--------------------------------------------------------------------------------------
sha512sums=('5a1bc5c6af6ad25cb8b2c446e75a774d2a615d4999ec3223d681c7b120d83e7cecd94f1ca549bac0802f5324e27e73cc5a6483ad410636c2f06f098b30b1b647')
